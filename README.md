# SwitchBoard
This switchboard is intended as modern 'plug-in' at the nav station of a yacht where it can be used to control all N2k and SignalK devices on board. 


## Description
This board is develop as a HAT for the Hatlabs Sailor HAT with ESP32. The main features of the board are:
- User interface for [SmartRelay](https://gitlab.com/BoatNet/smartrelay) board 
- Visual feedback of network components
- Supports N2k and SignalK
- rotary encoder for selection (2x)
- I2C OLED screen for feedback (2x) (both 128x32 and 182x64)
- RGB LEDs for feedback (2x)

Rendering of a rev 0.0.1 board:

Front side.
![Rev 0.0.1 board](PCB.png)

3D render of the board
![3D](Switchboard.png)


## Project status
This project is currently Work In Progress. First PCBs to be ordered.

This is a first iteration of the SwitchBoard. Finally the Switchboard should get a modular (both hardware and software) design where all sorts of switches, LEDs, lamps or displays can be used.
